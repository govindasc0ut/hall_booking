<?php

use App\Http\Controllers\Front\BookingController;
use App\Http\Controllers\Front\BranchController;
use App\Http\Controllers\Front\HallController;
use App\Http\Controllers\Front\LandingPageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

Route::get('/',[LandingPageController::class,'index'])->name('landingpage');

// branch
Route::resource('branches',BranchController::class);
// hall
Route::resource('halls',HallController::class);
// booking
Route::resource('bookings',BookingController::class);

Route::post('/bookings/fetchhalls',[BookingController::class,'fetchHalls'])->name('bookings.fetchhalls');
Route::post('/bookings/isavailable',[BookingController::class,'isAvailable'])->name('bookings.isavailable');
