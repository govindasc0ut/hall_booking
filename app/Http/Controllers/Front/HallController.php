<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\Hall;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $halls=Hall::get();
        return view('halls.index',[
            'halls'=>$halls,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branches=Branch::get();
        return view('halls.create',[
            'branches'=>$branches,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            DB::beginTransaction();
            $data=$this->validate($request,[
                'hall'=>'required|string|max:191',
                'branch_id'=>'required|numeric',
            ]);
             Hall::create($data);
             DB::commit();
            session()->flash('success','hall has been created successfully');
            return redirect()->back();
        }catch(Exception $exception){
            DB::rollBack();
            return redirect()->back()->with('danger', $exception->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       try
       {
        $hall=Hall::findOrFail($id);
        $branches=Branch::get();
        return view('halls.edit',[
            'hall'=>$hall,
            'branches'=>$branches,
        ]);
       }catch(Exception $exception){
        return redirect()->back()->with('danger', $exception->getMessage())->withInput();
    }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       try
       {
        $hall=Hall::findOrFail($id);
        DB::beginTransaction();
        $data=$this->validate($request,[
            'hall'=>'required|string|max:191',
            'branch_id'=>'required|numeric',
        ]);
        $hall->update($data);
        DB::commit();
        session()->flash('success','hall has been updated successfully');
        return redirect()->back();
       }catch(Exception $exception){
        DB::rollBack();
        return redirect()->back()->with('danger', $exception->getMessage())->withInput();
    }
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       try
       {
        $hall= Hall::findOrFail($id);
        $hall->delete();
        session()->flash('danger','hall has been deleted');
        return redirect()->back();
       }catch(Exception $exception){
        return redirect()->back()->with('danger', $exception->getMessage())->withInput();
    }
    }
}
