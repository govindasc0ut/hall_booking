<?php

namespace App\Http\Controllers\Front;

use Exception;
use App\Models\Branch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches=Branch::get();
        return view('branches.index',[
            'branches'=>$branches,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('branches.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            DB::beginTransaction();
            $data=$this->validate($request,[
                'branch'=>'required|string|max:191',
            ]);
             Branch::create($data);
             DB::commit();
            session()->flash('success','branch has been created successfully');
            return redirect()->back();
        }catch(Exception $exception){
            DB::rollBack();
            return redirect()->back()->with('danger', $exception->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       try
       {
        $branch=Branch::findOrFail($id);
        return view('branches.edit',[
            'branch'=>$branch,
        ]);
       }catch(Exception $exception){
        return redirect()->back()->with('danger', $exception->getMessage())->withInput();
    }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       try
       {
        $branch=Branch::findOrFail($id);
        DB::beginTransaction();
        $data=$this->validate($request,[
            'branch'=>'required|string|max:191',
        ]);
        $branch->update($data);
        DB::commit();
        session()->flash('success','branch has been updated successfully');
        return redirect()->back();
       }catch(Exception $exception){
        DB::rollBack();
        return redirect()->back()->with('danger', $exception->getMessage())->withInput();
    }
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       try
       {
        $branch= Branch::findOrFail($id);
        $branch->delete();
        session()->flash('danger','branch has been deleted');
        return redirect()->back();
       }catch(Exception $exception){
        return redirect()->back()->with('danger', $exception->getMessage())->withInput();
    }
    }
}
