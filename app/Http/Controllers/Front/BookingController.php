<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\Branch;
use App\Models\Hall;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookings = Booking::get();
        return view('bookings.index', [
            'bookings' => $bookings,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branches = Branch::get();
        $halls = Hall::get();
        return view('bookings.create', [
            'branches' => $branches,
            'halls' => $halls,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = $this->validate($request, [
                'booked_by' => 'required|string|max:191',
                'reason' => 'required|string|max:191',
                'date' => 'required|date',
                'start_time' => 'required|date_format:H:i',
                'end_time' => 'required|date_format:H:i',
                'branch_id' => 'required|numeric',
                'hall_id' => 'required|numeric',
            ]);
            Booking::create($data);
            DB::commit();
            session()->flash('success', 'booking has been created successfully');
            return redirect()->back();
        } catch (Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('danger', $exception->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $booking = Booking::findOrFail($id);
            $branches = Branch::get();
            $halls = Hall::get();
            return view('halls.edit', [
                'booking' => $booking,
                'branches' => $branches,
                'halls' => $halls,
            ]);
        } catch (Exception $exception) {
            return redirect()->back()->with('danger', $exception->getMessage())->withInput();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $booking = Booking::findOrFail($id);
            DB::beginTransaction();
            $data = $this->validate($request, [
                'booked_by' => 'required|string|max:191',
                'reason' => 'required|string|max:191',
                'date' => 'required|date',
                'start_time' => 'required|date_format:H:i',
                'end_time' => 'required|date_format:H:i',
                'branch_id' => 'required|numeric',
                'hall_id' => 'required|numeric',
            ]);
            $booking->update($data);
            DB::commit();
            session()->flash('success', 'booking has been updated successfully');
            return redirect()->back();
        } catch (Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('danger', $exception->getMessage())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $booking = Booking::findOrFail($id);
            $booking->delete();
            session()->flash('danger', 'booking has been deleted');
            return redirect()->back();
        } catch (Exception $exception) {
            return redirect()->back()->with('danger', $exception->getMessage())->withInput();
        }
    }

    public function fetchHalls(Request $request)
    {
        $branch = $request->post('branch');
        $halls = Hall::where('branch_id', $branch)->get();
        return response()->json($halls);
    }

    public function isAvailable(Request $request)
    {
        $branch = $request->post('branch');
        $hall = $request->post('hall');
        $date = $request->post('date');
        $start_time = $request->post('start_time');
        $end_time = $request->post('end_time');
        $booking = Booking::where('branch_id', $branch)
            ->where('hall_id', $hall)
            ->whereDate('date', date('y-m-d',strtotime($date)))
            ->whereDate('start_time','>=',date('H:i',strtotime($start_time)))
            ->orWhereDate('start_time','<=',date('H:i',strtotime($end_time)))
            ->orWhereDate('end_time','>=',date('H:i',strtotime($start_time)))
            ->orWhereDate('end_time','<=',date('H:i',strtotime($end_time)))
            ->get();
            return response()->json($booking);
    }
}
