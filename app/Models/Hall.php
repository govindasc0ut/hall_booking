<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hall extends Model
{
    use HasFactory;

    protected $table="halls";
    protected $fillable=['hall','branch_id'];


    public function branch()
    {
        return $this->belongsTo('App\Models\Branch');
    }
    public function bookings()
    {
        return $this->hasMany('App\Models\Booking');
    }
}
