<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    protected $table="bookings";
    protected $fillable=['booked_by','branch_id','hall_id','date','start_time','end_time','reason'];


    public function branch()
    {
        return $this->belongsTo('App\Models\Branch');
    }

    public function hall()
    {
        return $this->belongsTo('App\Models\Hall');
    }
}
