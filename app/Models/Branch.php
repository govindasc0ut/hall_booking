<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    use HasFactory;
    
    protected $table="branches";
    protected $fillable=['branch'];

    public function halls()
    {
        return $this->hasMany('App\Models\Hall');
    }

    public function bookings()
    {
        return $this->hasMany('App\Models\Booking');
    }
}
