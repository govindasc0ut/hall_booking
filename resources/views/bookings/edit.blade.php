@include('partial.header')
@include('partial.navbar')
<div class="container">
    <div class="card">
            <div class="card-header">
                <h3>Update  subscribes</h3>
                <a href="{{route('subscribes.index')}}" class="btn btn-success">All subscribes</a>
            </div>
            <div>
              @include('_partial.notification')
            </div>
            <div class="card-body">
            <form action="{{route('subscribes.update',$subscribe->id)}}" method="post">
              @csrf
              @method('PUT')
                <div class="mb-3">
                    <label for="person_id">Person *</label>
                    <select name="person_id" class="form-control lead">
                        @forelse($persons as $person)
                            <option value="{{ $person->id }}" class="form-control lead">{{ $person->name }}</option>
                        @empty
                            <option disabled selected>No person</option>
                        @endforelse
                    </select>
                </div>
                <div class="mb-3">
                    <label for="package_id">Package *</label>
                    <select name="package_id" class="form-control lead">
                        @forelse($packages as $package)
                            <option value="{{ $package->id }}" class="form-control lead">{{ $package->name }}</option>
                        @empty
                            <option disabled selected>No package</option>
                        @endforelse
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            </div>
    </div>
</div>
@include('partial.footer')