@include('partials.header')
@include('partials.navbar')
<div class="container">
    <div class="card">
        <div class="card-header">
            <h3>Add bookings</h3>
            <a href="{{ route('bookings.index') }}" class="btn btn-success">All bookings</a>
        </div>
        <div class="availability">
              @include('_partial.notification')
        </div>
        <div class="card-body">
        <form action="{{route('bookings.store')}}" method="post">
          @csrf
          <div class="mb-3">
            <label for="branch_id">Branch *</label>
            <select name="branch_id" class="form-control lead" id="branch">
                <option disabled selected>Select branch</option>
                @forelse($branches as $branch)
                    <option value="{{ $branch->id }}"  class="form-control lead">{{ $branch->branch }}</option>
                @empty
                    <option disabled selected>No branch</option>
                @endforelse
            </select>
        </div>
        <div class="mb-3">
            <label for="hall_id">Hall *</label>
            <select name="hall_id" class="form-control lead" id="hall">
                
            </select>
        </div>
        <div class="mb-3">
            <label for="" class="form-label">booked_by</label>
            <input type="text" name="booked_by" class="form-control">
        </div>
        <div class="mb-3">
            <label for="" class="form-label">date</label>
            <input type="date" id="date" name="date" class="form-control">
        </div>
        <div class="mb-3">
            <label for="" class="form-label">start_time</label>
            <input type="time" id="start_time" name="start_time" class="form-control">
        </div>
        <div class="mb-3">
            <label for="" class="form-label">end_time</label>
            <input type="time" id="end_time" name="end_time" class="form-control">
        </div>
        <div class="mb-3">
            <label for="" class="form-label">Reason</label>
            <textarea name="reason" class="form-control"></textarea>
        </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
      $('#branch').change(function(){
          $('#hall').html('');
        var branch=$('#branch').val();
        var base=$('base').attr('href');
        var url="{{route('bookings.fetchhalls')}}";
        var data={
          branch:branch,
        }
        $.ajax({
           type:"POST",
           headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
           data:data,
           url:url,
           success: handleData,
        });
        function handleData(res,resText,jqxhr){
         if(jqxhr.status==200)
         {
           console.log(res);
           $.each(res,function(index,element){
            var abc=` <option value="`+element.id+`" class="form-control lead">`+element.hall+`</option>`
            $('#hall').append(abc);
        });

         }
        }
      });
      $('#end_time').change(function(){
          var branch=$('#branch').val();
          var hall=$('#hall').val();
          var date=$('#date').val();
          var start_time=$('#start_time').val();
          var end_time=$('#end_time').val();
          var base=$('base').attr('href');
        var url="{{route('bookings.isavailable')}}";
        var data={
          branch:branch,
          hall:hall,
          date:date,
          start_time:start_time,
          end_time:end_time,
        }
        $.ajax({
           type:"POST",
           headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
           data:data,
           url:url,
           success: handleResponse,
        });
        function handleResponse(res,resText,jqxhr){
         if(jqxhr.status==200)
         {
           console.log(res);
           if(res.length >0)
           {
               window.alert('not available');
           }
           else{
             window.alert('available');
           }

         }
        }
      });
    });
  </script>
@include('partials.footer')
