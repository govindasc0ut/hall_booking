@include('partials.header')
@include('partials.navbar')
<div class="container">
    <div class="card">
        <div class="card-header">
            <h3>List of bookings</h3>
            <a href="{{ route('bookings.create') }}" class="btn btn-success">Add new</a>
        </div>
        <div>
              @include('_partial.notification')
            </div>
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Branch</th>
                        <th scope="col">Hall</th>
                        <th scope="col">Date</th>
                        <th scope="col">Start Time</th>
                        <th scope="col">End Time</th>
                        <th scope="col">Reason</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($bookings as $booking)
                    <tr>
                    <th scope="row">{{$loop->index+1}}</th>
                        <td>{{$booking->branch->branch}}</td>
                        <td>{{$booking->hall->hall}}</td>
                        <td>{{$booking->date}}</td>
                        <td>{{$booking->start_time}}</td>
                        <td>{{$booking->end_time}}</td>
                        <td>{{$booking->reason}}</td>
                        <td>
                            <div class="d-flex">
                                <form action="{{ route('bookings.destroy', $booking->id) }}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger ">Delete</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@include('partials.footer')
