@include('partials.header')
@include('partials.navbar')
<div class="container">
    <div class="card">
        <div class="card-header">
            <h3>List of halls</h3>
            <a href="{{ route('halls.create') }}" class="btn btn-success">Add new</a>
        </div>
        <div>
            @include('_partial.notification')
          </div>
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Branch</th>
                        <th scope="col">Hall</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($halls as $hall)
                    <tr>
                        <th scope="row">{{ $loop->index + 1 }}</th>
                        <td>{{ $hall->branch->branch }}</td>
                        <td>{{ $hall->hall }}</td>
                        <td>
                            <div class="d-flex">
                                <a href="{{route('halls.edit',$hall->id)}}" class="btn btn-info">Edit</a>
                            &nbsp;&nbsp;
                            <form action="{{route('halls.destroy',$hall->id)}}" method="post">
                              @csrf
                              @method('delete')
                              <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@include('partials.footer')
