@include('partials.header')
@include('partials.navbar')
<div class="container">
    <div class="card">
        <div class="card-header">
            <h3>Add halls</h3>
            <a href="{{ route('halls.index') }}" class="btn btn-success">All halls</a>
        </div>
        <div>
            @include('_partial.notification')
        </div>
        <div class="card-body">
            <form action="{{ route('halls.store') }}" method="post">
                @csrf
                <div class="mb-3">
                    <label for="branch_id">Branch *</label>
                    <select name="branch_id" class="form-control lead">
                        @forelse($branches as $branch)
                            <option value="{{ $branch->id }}" class="form-control lead">{{ $branch->branch }}</option>
                        @empty
                            <option disabled selected>No branch</option>
                        @endforelse
                    </select>
                </div>
                <div class="mb-3">
                    <label for="" class="form-label">Hall</label>
                    <input type="text" name="hall" class="form-control">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@include('partials.footer')
