<div class="row">
    <div class="col-12 mt-3">
        @foreach ($errors as $error)
            <div class="alert alert-danger">
                {{$error}}
            </div>
        @endforeach
    </div>
</div>