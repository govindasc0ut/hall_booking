@include('partials.header')
@include('partials.navbar')
<div class="container">
    <div class="card">
            <div class="card-header">
                <h3>Update  Branch</h3>
                <a href="{{route('branches.index')}}" class="btn btn-success">All Branches</a>
            </div>
            <div>
              @include('_partial.notification')
            </div>
            <div class="card-body">
            <form action="{{route('branches.update',$branch->id)}}" method="post">
              @csrf
              @method('PUT')
                    <div class="mb-3">
                      <label for="" class="form-label">Branch</label>
                    <input type="text" name="branch" class="form-control" value="{{$branch->branch}}">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
            </div>
    </div>
</div>
@include('partials.footer')